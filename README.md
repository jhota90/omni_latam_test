### HOW TO RUN THIS PROJECT

- **WITH DOCKER** *(into folder)*:
```
docker-compose build
docker-compose up -d
docker-compose exec sales_web python manage.py makemigrations products
docker-compose exec sales_web python manage.py makemigrations orders
docker-compose exec sales_web python manage.py makemigrations payments
docker-compose exec sales_web python manage.py makemigrations shipments
docker-compose exec sales_web python manage.py makemigrations notifications
docker-compose exec sales_web python manage.py migrate
docker-compose exec sales_web python manage.py createsuperuser
```

- **WITHOUT DOCKER** *(you should have python 3.8 installed)*
```
cd app
python -m pip install -r requirments.txt
python manage.py makemigrations products
python manage.py makemigrations orders
python manage.py makemigrations payments
python manage.py makemigrations shipments
python manage.py makemigrations notifications
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver 0:8000
```

Now that the servers running, visit http://127.0.0.1:8000/admin with your Web browser

### HOW TO RUN TEST

- **WITH DOCKER**
```
docker-compose exec sales_web python manage.py test sales.modules.<module_name>
```

- **WITHOUT DOCKER** *(you should have python 3.8 installed)*
```
python manage.py test sales.modules.<module_name>
```