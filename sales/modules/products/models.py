from django.db import models
from django.conf import settings

class Product(models.Model):
    name = models.CharField(verbose_name='Name', max_length=50, null=False, blank=True)
    description = models.TextField(verbose_name='Description', max_length=255, null=True, blank=True)
    category = models.CharField(verbose_name='Category', max_length=20, null=True, blank=True)

    def __str__(self):
        return str(self.id) + " - "  + str(self.name)