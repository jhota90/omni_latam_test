from rest_framework.test import APITestCase
from rest_framework import status
from sales.test_tools.TestTools import TestTools


class ProductTestCase(APITestCase):
    
    products_url = '/products/'
    
    def setUp(self) -> None:
        self.token = TestTools.getToken()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        product_list = [
            {
            "name":"Tennis orange ball",
            "category":"sports",
            "description":"Tennis orange ball"
            },
            {
            "name":"Tennis green ball",
            "category":"sports",
            "description":"Tennis green ball"
            },
            {
            "name":"Soccer Nike ball",
            "category":"sports",
            "description":"Soccer Nike ball"
            },
        ]
        for a_product in product_list:
            self.client.post(self.products_url, data=a_product)
    
    def test_product_list_authenticated(self):
        response = self.client.get(self.products_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_product_create(self):
        response = self.client.post(self.products_url, data={
            "name":"Tennis ball x3",
            "category":"sports",
            "description":"Tennis green ball x3"
            })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_product_detail_retrieve(self):
        retrieve_url =  self.products_url + "1/"
        response = self.client.get(retrieve_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_product_list_unauthenticated(self):
        self.client.credentials(user=None)
        response = self.client.get(self.products_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)