from django.contrib import admin
from sales.modules.payments.models import Payment, OrderPayment

class PaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'customer', 'amount', 'created_at')
    list_filter = ['customer', 'created_at']
    search_fields = ['customer', 'created_at']


class OrderPaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'payment', 'order', 'amount', 'created_at')
    list_filter = ['payment', 'order']
    search_fields = ['payment', 'order']


admin.site.register(Payment, PaymentAdmin)
admin.site.register(OrderPayment, OrderPaymentAdmin)