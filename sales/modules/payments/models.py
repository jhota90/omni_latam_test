from django.db import models
from django_currentuser.db.models import CurrentUserField


class Payment(models.Model):
	customer = CurrentUserField()
	amount = models.FloatField('Amount', blank=True, null=True)
	created_at = models.DateTimeField('Created at', auto_now_add=True)

	def __str__(self):
		return "PAY-"+str(self.id).zfill(9)


class OrderPayment(models.Model):
	payment = models.ForeignKey("payments.Payment", on_delete=models.CASCADE, null=True, blank=True)
	order = models.ForeignKey("orders.Order", on_delete=models.CASCADE, null=True, blank=True)
	amount = models.FloatField('Amount', blank=True, null=True)
	created_at = models.DateTimeField('Created at', auto_now_add=True)
