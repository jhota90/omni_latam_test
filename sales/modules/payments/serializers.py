from rest_framework import serializers
from .models import Payment, OrderPayment

class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = ['id', 'customer', 'amount', 'created_at']
        read_only_fields = ['id', 'customer']
        

class OrderPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderPayment
        fields = ['id', 'payment', 'order', 'amount', 'created_at']
        read_only_fields = ['id']