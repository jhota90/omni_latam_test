from django.urls import include, path
from rest_framework import routers
from .views import PaymentViewSet, OrderPaymentViewSet

payment_router = routers.DefaultRouter()
payment_router.register('', PaymentViewSet)

order_payment_router = routers.DefaultRouter()
order_payment_router.register('', OrderPaymentViewSet)

urlpatterns = [
    path('payments/', include(payment_router.urls)),
    path('order_payments/', include(order_payment_router.urls)),
]