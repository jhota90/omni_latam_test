from .serializers import PaymentSerializer, OrderPaymentSerializer
from rest_framework import viewsets
from .models import Payment, OrderPayment


class PaymentViewSet(viewsets.ModelViewSet):
   
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    
class OrderPaymentViewSet(viewsets.ModelViewSet):
   
    queryset = OrderPayment.objects.all()
    serializer_class = OrderPaymentSerializer