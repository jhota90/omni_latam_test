from django.db import models

class Shipment(models.Model):
    STATUS_OPTIONS = (
        ('sent', 'SENT'),
        ('delivered', 'DELIVERED'),
    )
    created_at = models.DateTimeField(verbose_name='Created at', auto_now_add=True)
    status = models.CharField(verbose_name='Status', blank=True, null=True, choices=STATUS_OPTIONS, max_length=9, default='sent')

    def __str__(self):
        return "SHP-"+str(self.id).zfill(9)


class ShipmentProduct(models.Model):
    shipment = models.ForeignKey('shipments.Shipment', on_delete=models.DO_NOTHING, null=True, blank=True)
    order_product = models.ForeignKey('orders.OrderProduct', on_delete=models.DO_NOTHING, null=True, blank=True)
    quantity = models.FloatField(verbose_name="Quantity")
    delivered_at = models.DateTimeField(verbose_name='Delivered at', null=True)
