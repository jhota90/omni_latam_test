from django.contrib import admin
from sales.modules.shipments.models import Shipment, ShipmentProduct

class ShipmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'created_at')
    list_filter = ['id', 'status']
    search_fields = ['id', 'status']


class ShipmentProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'shipment', 'order_product', 'quantity', 'delivered_at')
    list_filter = ['order_product']
    search_fields = ['order_product']


admin.site.register(Shipment, ShipmentAdmin)
admin.site.register(ShipmentProduct, ShipmentProductAdmin)