from rest_framework import serializers
from .models import Shipment, ShipmentProduct

class ShipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = ['id', 'status', 'created_at']
        read_only_fields = ['id']
        

class ShipmentProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShipmentProduct
        fields = ['id', 'shipment', 'order_product', 'quantity', 'delivered_at']
        read_only_fields = ['id']