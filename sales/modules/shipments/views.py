from .serializers import ShipmentSerializer, ShipmentProductSerializer
from rest_framework import viewsets
from .models import Shipment, ShipmentProduct


class ShipmentViewSet(viewsets.ModelViewSet):
   
    queryset = Shipment.objects.all()
    serializer_class = ShipmentSerializer
    
class ShipmentProductViewSet(viewsets.ModelViewSet):
   
    queryset = ShipmentProduct.objects.all()
    serializer_class = ShipmentProductSerializer