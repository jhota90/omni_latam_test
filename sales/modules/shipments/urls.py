from django.urls import include, path
from rest_framework import routers
from .views import ShipmentViewSet, ShipmentProductViewSet

shipment_router = routers.DefaultRouter()
shipment_router.register('', ShipmentViewSet)

shipment_product_router = routers.DefaultRouter()
shipment_product_router.register('', ShipmentProductViewSet)

urlpatterns = [
    path('shipments/', include(shipment_router.urls)),
    path('shipment_products/', include(shipment_product_router.urls)),
]