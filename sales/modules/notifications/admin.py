from django.contrib import admin
from sales.modules.notifications.models import Notification

class NotificationAdmin(admin.ModelAdmin):
    list_display = ('id', 'shipment', 'subject', 'body', 'created_at')
    list_filter = ['subject', 'shipment']
    search_fields = ['subject', 'shipment', 'created_at']

admin.site.register(Notification, NotificationAdmin)