from django.db import models

class Notification(models.Model):
    created_at = models.DateTimeField(verbose_name='Created at', auto_now_add=True)
    shipment = models.ForeignKey('shipments.Shipment', on_delete=models.DO_NOTHING, null=True, blank=True)
    subject = models.CharField(verbose_name='Subject', max_length=50, null=True, blank=True)
    body = models.TextField(verbose_name='Body', null=True, blank=True)

