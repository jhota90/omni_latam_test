from django.db import models
from django_currentuser.db.models import CurrentUserField

class Order(models.Model):
    STATUS_OPTIONS = (
        ('new', 'NEW'),
        ('paid', 'PAID'),
        ('sent', 'SENT'),
    )
    customer = CurrentUserField()
    created_at = models.DateTimeField(verbose_name='Created at', auto_now_add=True)
    shipping_address = models.CharField(verbose_name='Address', blank=True, null=True, max_length=255)
    status = models.CharField(verbose_name='Status', blank=True, null=True, choices=STATUS_OPTIONS, max_length=4, default='new')

    def __str__(self):
        return "ORD-"+str(self.id).zfill(9)


class OrderProduct(models.Model):
    order = models.ForeignKey('orders.Order', on_delete=models.DO_NOTHING, null=True, blank=True)
    product = models.ForeignKey('products.Product', on_delete=models.DO_NOTHING, null=True, blank=True)
    quantity = models.FloatField(verbose_name="Quantity") 
    amount = models.FloatField(verbose_name="Amount") 
