from .serializers import OrderSerializer, OrderProductSerializer
from rest_framework import viewsets
from .models import Order, OrderProduct


class OrderViewSet(viewsets.ModelViewSet):
   
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    
class OrderProductViewSet(viewsets.ModelViewSet):
   
    queryset = OrderProduct.objects.all()
    serializer_class = OrderProductSerializer