from rest_framework.test import APITestCase
from rest_framework import status
from sales.test_tools.TestTools import TestTools


class OrderTestCase(APITestCase):
    
    products_url = '/products/'
    orders_url = '/sales/orders/'
    order_products_url = '/sales/order_products/'
    
    def setUp(self) -> None:
        self.token = TestTools.getToken()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        
        # Create products
        product_list = [
            {
            "name":"Tennis orange ball",
            "category":"sports",
            "description":"Tennis orange ball"
            },
            {
            "name":"Tennis green ball",
            "category":"sports",
            "description":"Tennis green ball"
            },
            {
            "name":"Soccer Nike ball",
            "category":"sports",
            "description":"Soccer Nike ball"
            },
        ]
        for a_product in product_list:
            self.client.post(self.products_url, data=a_product)
        
        # Create orders
        order_list = [
            {
            "status":"new",
            "shipping_address":"1996 Wolf Street"
            },
            {
            "status":"new",
            "shipping_address":"1997 Wolf Street"
            },
            {
            "status":"new",
            "shipping_address":"1998 Wolf Street"
            },
        ]
        order_products_list = [
            {
            "quantity":3,
            "amount":300
            },
            {
            "quantity":2,
            "amount":500
            },
            {
            "quantity":1,
            "amount":150
            },
        ]
        for a_order in order_list:
            self.client.post(self.orders_url, data=a_order)
        
        for a_order in range(1,4):
            for a_product in range(1,4):
                for a_desc in order_products_list:
                    a_desc['order'] = a_order
                    a_desc['product'] = a_product
                    self.client.post(self.order_products_url, data=a_desc)
    
    def test_order_list_authenticated(self):
        response = self.client.get(self.orders_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_order_create(self):
        response = self.client.post(self.orders_url, data={
            "status":"new",
            "shipping_address":"1779 Wolf Street"
            })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_order_detail_retrieve(self):
        retrieve_url =  self.orders_url + "1/"
        response = self.client.get(retrieve_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_order_product_detail_retrieve(self):
        retrieve_url =  self.order_products_url + "1/"
        response = self.client.get(retrieve_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_order_list_unauthenticated(self):
        self.client.credentials(user=None)
        response = self.client.get(self.orders_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)