from django.contrib import admin
from sales.modules.orders.models import Order, OrderProduct

class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'customer', 'created_at', 'shipping_address', 'status')
    list_filter = ['customer', 'status']
    search_fields = ['customer', 'status']


class OrderProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'product', 'quantity', 'amount')
    list_filter = ['product']
    search_fields = ['product']


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderProduct, OrderProductAdmin)