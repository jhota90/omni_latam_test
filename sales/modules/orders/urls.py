from django.urls import include, path
from rest_framework import routers
from .views import OrderViewSet, OrderProductViewSet

order_router = routers.DefaultRouter()
order_router.register('', OrderViewSet)

order_product_router = routers.DefaultRouter()
order_product_router.register('', OrderProductViewSet)

urlpatterns = [
    path('orders/', include(order_router.urls)),
    path('order_products/', include(order_product_router.urls)),
]