from rest_framework import serializers
from .models import Order, OrderProduct

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['id', 'customer', 'created_at', 'shipping_address', 'status']
        read_only_fields = ['id', 'customer']
        

class OrderProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderProduct
        fields = ['id', 'order', 'product', 'quantity', 'amount']
        read_only_fields = ['id']