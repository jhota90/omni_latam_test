class TestTools:
    
    @staticmethod
    def getToken():
        from rest_framework.authtoken.models import Token
        from django.contrib.auth.models import User
        user = User.objects.create_user(username='testing', password='testing')
        token = Token.objects.create(user=user)
        return token