"""sales URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views
from sales.modules.users.views import SignOutView, SignUpView

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('signin/', views.obtain_auth_token),
    path('signout/', SignOutView.as_view()),
    path('signup/', SignUpView.as_view()),
    path('products/', include('sales.modules.products.urls')),
    path('sales/', include('sales.modules.orders.urls')),
    path('payments/', include('sales.modules.payments.urls')),
    path('shipments/', include('sales.modules.shipments.urls')),
    path('notifications/', include('sales.modules.notifications.urls')),
    #path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
